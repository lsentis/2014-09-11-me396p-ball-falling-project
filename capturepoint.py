#!/usr/bin/python
import math as mth
import matplotlib.pyplot as plt # modules
import turtle
import time

# init
xx = [1]
dx = [0.5]
TT = 0.03

# model and solver
while mth.fabs( dx[-1] ) > 0.01:
	new_item = -xx[-1]
	dx.append( new_item )
	new_item = xx[-1] + dx[-1] * TT
	xx.append( new_item )

# plotting
legend_size = 8
plt.clf()
plt.plot( xx )
plt.xlabel('iteration')
plt.ylabel('[m]')
plt.title('Capture point')

# print plot
wadjust = 0.4
hadjust = 0.6
plt.subplots_adjust(wspace = wadjust)
plt.subplots_adjust(hspace = hadjust)
plt.savefig('figure2.png')

# turtle program
win = turtle.Screen()
win.bgcolor("white")
tess = turtle.Turtle()
tess.shape("turtle")
tess.color("blue")
tess.shapesize(5, 5, 2)
tess.pencolor("white")

num = 200
tess.color("red")
ss = tess.stamp()
time.sleep(1)
tess.penup()
tess.hideturtle()

tess.color("blue")
for item in xx:
	tess.setx(item*num)
	ss = tess.stamp()
	tess.showturtle()
	time.sleep(0.01)
	tess.clearstamp(ss)	

time.sleep(5)
# win.exitonclick()
