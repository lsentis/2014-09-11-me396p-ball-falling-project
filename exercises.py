#!/usr/bin/python
print 'i am not a robot'

def ps():
	print 'something'

# this is a comment
def foo(param, *nums, **dict):
	print 'param is:	', param
	print 'list of numbers is:', nums
	print 'dictionary is:', dict

foo(1,2,3,4,5,cell=512111111,home=512222222)

# lists
icebucket_nominees = ['MITs Rafael Reif','Harvards Drew Faust', 'Turkeys Akinci Humanoid Robot']
print 'num of nominees:', len(icebucket_nominees)
ii = 1
for nominee in icebucket_nominees:
	print 'nominee', ii, 'is', nominee
	ii += 1

icebucket_nominees.sort()
print 'sorted list', icebucket_nominees

# number list
num_list = [4,5,2,6]
print 'number list:', num_list
num_list.sort()
print num_list

# data manipulation
print 'who is the 2nd nominee?', icebucket_nominees[1]
print 'first two nominees', icebucket_nominees[0:2]
print 'last two nominees', icebucket_nominees[1:]

# logic
print 'MITs Rafael Reif' in icebucket_nominees


