print 'i am not a robot'

def ps():
	print 'something'

ps()

# this is a comment

# this is another comment
def foo(param, *nums, **dict):
	print param
	print nums
	print dict

foo(1,2,3,4,cell=5121111111,home=512222222)

# lists
icebucket_nominees = ['MIT Rafael Reif', 'UT Powels', 'Harvard Faust', 'Turkey Akinci Robot']
print 'num of nominees', len(icebucket_nominees)

for item in icebucket_nominees:
	print 'nominee is:', item

icebucket_nominees.sort()
print 'sorted list', icebucket_nominees

# create a list of numbers 
num_lists = [9, 6, 3, 8]
print num_lists
list1 = num_lists
print list1

# passing by reference vs. passing by value
list2 = num_lists[:]
list2.sort()
print 'list2: ', list2
print 'list1: ', list1

# data manipulation
print '2nd nominee', icebucket_nominees[1]

icebucket_nominees.append('test entry')
print icebucket_nominees

# student exercise
print 'first 2 entries', icebucket_nominees[0:2]
print 'last 2 entries',  icebucket_nominees[-2:]

# logic
print 3 in num_lists
