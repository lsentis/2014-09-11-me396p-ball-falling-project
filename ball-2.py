#!/usr/bin/python
import matplotlib.pyplot as plt
import turtle
import time

# global variables
gg = 9.81
TT = 0.01

# model
ddz = -gg # m/s^2

# solver 1 time
# dz0 = 10 # m/s
# z0 = 1 # m
# dz = dz0 + ddz * TT
# zz = z0 + dz * TT + 0.5 * ddz * TT**2
# print zz

# initial values
dz = [10]
zz = [1]

# solver
while zz[-1] > -9:
	dz.append( dz[-1] + ddz * TT )
	zz.append( zz[-1] + dz[-1] * TT + 0.5 * ddz * TT**2 )

# data visualization
plt.clf()
plt.plot(zz)
plt.xlabel('iteration')
plt.ylabel('[m]')
plt.title('Falling Ball')
# plt.show()

# file data management
plt.subplots_adjust(wspace = 0.4)
plt.subplots_adjust(hspace = 0.6)
plt.savefig('figure.png')

# animation
win = turtle.Screen()
win.bgcolor("white")
tess = turtle.Turtle()
tess.shape("turtle")
tess.color("blue")
tess.shapesize(5,5,2)
tess.pencolor("white")

# move turtle to initial position
tess.sety( zz[0] * 50)
tess.penup()
tess.left(90)
ss = tess.stamp()
time.sleep(1)
tess.clearstamp(ss)

# iteration
# exercise: draw each position of the turtle 
# according to zz and place a turtle stamp
for entry in zz:
	tess.sety( entry * 50 )

time.sleep(5)

