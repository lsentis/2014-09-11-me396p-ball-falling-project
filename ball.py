#!/usr/bin/python
import matplotlib.pyplot as plt # modules
import turtle
import time

# global variables
gg = -9.81 # double notation for efficient search
TT = 0.01

# model
ddz = gg # m/s^2

# # solver 1 time
# dz0 = 10 # m/s
# z0 = 1 # m
# dz = dz0 + ddz * TT
# zz = z0 + dz * TT + 0.5 * ddz * TT ** 2
# print zz

# initial values
dz = [10]
zz = [1]

# solver until it hits the floor
while zz[-1] > -6.5:
	new_item = dz[-1] + ddz * TT
	dz.append( new_item )
	new_item = zz[-1] + dz[-1] * TT + ddz * TT**2
	zz.append( new_item )

# plotting
legend_size = 8
plt.clf()
plt.plot( zz )
plt.xlabel('iteration')
plt.ylabel('[m]')
plt.title('Balls Height')

# print plot
wadjust = 0.4
hadjust = 0.6
plt.subplots_adjust(wspace = wadjust)
plt.subplots_adjust(hspace = hadjust)
plt.savefig('figure.png')

# turtle program
win = turtle.Screen()
win.bgcolor("white")
tess = turtle.Turtle()
tess.shape("turtle")
tess.color("blue")
tess.shapesize(5, 5, 2)
tess.pencolor("white")

# tess.hideturtle()
num = 55
tess.sety(zz[0]*num)
tess.penup()
tess.left(90)
ss = tess.stamp()
time.sleep(1)
tess.clearstamp(ss)	

size = 20
for item in zz:
	tess.sety(item*num)
	ss = tess.stamp()
	time.sleep(0.01)
	tess.clearstamp(ss)	

time.sleep(5)
